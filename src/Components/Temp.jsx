import React, { useState, useEffect } from "react";
import "../Css/TempCss.css";

const Temp = () => {
  const [city, setCity] = useState();
  const [search, setSearch] = useState("Karachi");

  useEffect(() => {
    const fetchApi = async () => {
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${search}&units=metric&appid=cee268c533e7c4c35716efc85d8a25d4`;
      const res = await fetch(url);
      const resJson = await res.json();
      console.log(resJson);
      setCity(resJson.main);
    };
    fetchApi();
  }, [search]);

  return (
    <>
      <div className="temp">
        <div className="card">
          <div className="container">
            <input
              type="search"
              className="inputField"
              value={search}
              onChange={(event) => {
                setSearch(event.target.value);
              }}
            ></input>
          </div>
          <br />
          <br />
          <br />
          {!city ? (
            <p className="no"> No data found</p>
          ) : (
            <div className="value">
              <h2 className="cen">
                <i className="fa fa-street-view" />
                {search}
              </h2>

              <br />
              <br />
              <h1 className="tempT">{city.temp} °C</h1>
              <h3 className="temp-min">
                Min: {city.temp} °C | Max: {city.temp} °C
              </h3>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Temp;
